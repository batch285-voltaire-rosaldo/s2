import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Input year to be check if a leap year: ");
        int leapYear = numberScanner.nextInt();

        if(leapYear % 4 == 0){
            System.out.println(leapYear + " is a leap year");
        }else{
            System.out.println(leapYear + " is NOT a leap year");
        }

        int[] intPrime = new int[5];
        intPrime[0] = 2;
        intPrime[1] = 3;
        intPrime[2] = 5;
        intPrime[3] = 7;
        intPrime[4] = 11;

        System.out.println("The first prime number is: " + intPrime[0]);
        System.out.println("The second prime number is: " + intPrime[1]);
        System.out.println("The third prime number is: " + intPrime[2]);
        System.out.println("The fourth prime number is: " + intPrime[3]);System.out.println("The fifth prime number is: " + intPrime[4]);

        String[] heroes = {"Axe", "CM", "Void", "Juggernaut"};
        System.out.println("My heroes are: " + Arrays.toString(heroes));

        HashMap<String,Integer> inventory = new HashMap<>(){
            {
                put("toothpaste", 16);
                put("toothbrush", 25);
                put("soap", 13);
            }
        };

        System.out.println("Our current inventory consists of: " + inventory);

    }
}